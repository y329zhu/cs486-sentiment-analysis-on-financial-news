# CS486-sentiment-analysis-on-financial-news

CS 486 project for Spring 2020.
Topic: Sentiment Analysis on Financial News

## Data Preprocessing

## Perform Sentiment Analysis
- Load CS486-D3.ipynb into Jupyter notebook or Google Colab
- Click `run all` in runtime
- Upload 'all-full.csv', 'container.csv' and model1
